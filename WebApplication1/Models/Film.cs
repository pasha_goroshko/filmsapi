﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApplication1.Models
{
    public class FilmModel
    {
        /// <summary>
        /// Уникальный идентификатор
        /// </summary>
        public string Id { get; set; }
        /// <summary>
        /// Наименование
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// Дата премьеры
        /// </summary>
        public int Year { get; set; }
        /// <summary>
        /// Описание 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// Автор/Режисер
        /// </summary>
        public string Author { get; set; }
        /// <summary>
        /// Жанр
        /// </summary>
        public Genre Genre { get; set; }
        /// <summary>
        /// Альфа 2 код страны(RU,US,BY)
        /// </summary>
        public string CountryAlpha2Code { get; set; }
        /// <summary>
        /// Продолжительность
        /// </summary>
        public string Duration { get; set; }

        public string Image { get; set; }

    }

    public class FilmCreateModel
    {
        public string Title { get; set; }
        public int Year { get; set; }
        public string Description { get; set; }
        public Genre Genre { get; set; }
        public string Author { get; set; }
        public string CountryAlpha2Code { get; set; }
        public string Duration { get; set; }

        public string Image { get; set; }
        //public TimeSpan Duration { get; set; }
        //public IFormFile Image { get; set; }
    }

    public enum Genre
    {
        /// <summary>
        /// Боевик
        /// </summary>
        Action,
        /// <summary>
        /// Вестерн
        /// </summary>
        Western,
        /// <summary>
        /// Детектив
        /// </summary>
        Detective,
        /// <summary>
        /// Драма
        /// </summary>
        Drama,
        /// <summary>
        /// Исторический
        /// </summary>
        Historical,
        /// <summary>
        /// Комедия
        /// </summary>
        Comedy
    }
}
