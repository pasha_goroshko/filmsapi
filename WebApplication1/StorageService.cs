﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml.Serialization;
using WebApplication1.Models;

namespace WebApplication1
{
    public class StorageService
    {
        private static readonly string file = "films.xml";

        public List<FilmModel> GetFilms()
        {
            if (!File.Exists(file))
                return new List<FilmModel>();

            var serializer = new XmlSerializer(typeof(List<FilmModel>));

            using (var reader = new StreamReader(file))
            {
                return (List<FilmModel>)serializer.Deserialize(reader);
            }
        }

        public void Save(List<FilmModel> films)
        {
            if (File.Exists(file))
                File.Delete(file);

            // передаем в конструктор тип класса
            var serializer = new XmlSerializer(typeof(List<FilmModel>));

            using (var writer = new StreamWriter(file, false))
            {
                serializer.Serialize(writer, films);
            }
        }
    }
}
