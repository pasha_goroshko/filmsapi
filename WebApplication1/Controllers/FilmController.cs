﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    [Route("api/film")]
    [ApiController]
    public class FilmController : ControllerBase
    {
        private readonly StorageService storageService = new StorageService();
        private readonly IHostingEnvironment hostingEnvironment;
        public FilmController(IHostingEnvironment hostingEnvironment)
        {
            this.hostingEnvironment = hostingEnvironment;
        }

        // GET: api/Film
        // GET: api/Film?genre=2
        [HttpGet]
        public PagedList<FilmModel> Get(string title = null, int? year = null, string author = null, int? genre = null, string country = null, int page = 0, int pageSize = 10)
        {
            var films = storageService.GetFilms();

            var searchRes = films.Where(x =>
            (title == null ? true : x.Title.Contains(title.Trim(), StringComparison.OrdinalIgnoreCase))
            && (year == null ? true : x.Year == year.Value)
            && (author == null ? true : x.Author.Contains(author.Trim(), StringComparison.OrdinalIgnoreCase))
            && (genre == null ? true : x.Genre == (Genre)genre.Value)
            && (country == null ? true : x.CountryAlpha2Code.Equals(country.Trim(), StringComparison.OrdinalIgnoreCase))
            );

            return new PagedList<FilmModel>(searchRes, page, pageSize);
        }

        // GET: api/Film/5
        [HttpGet("{id}", Name = "Get")]
        public FilmModel Get(string id)
        {
            var films = storageService.GetFilms();
            return films.FirstOrDefault(x => x.Id == id);
        }

        // POST: api/Film
        [HttpPost]
        public void Post([FromBody] FilmCreateModel film)
        {
            var films = storageService.GetFilms();
            if (films.Any(x => x.Title.Equals(film.Title, StringComparison.InvariantCultureIgnoreCase)))
                throw new ArgumentException($"Фильм '{film.Title}' уже существует.");

            films.Add(new FilmModel
            {
                Id = Guid.NewGuid().ToString(),
                Title = film.Title,
                Description = film.Description,
                Genre = film.Genre,
                Year = film.Year,
                Author = film.Author,
                CountryAlpha2Code = film.CountryAlpha2Code,
                Duration = film.Duration,
                Image = film.Image ?? "no_product.png"
            });

            storageService.Save(films);
        }

        // PUT: api/Film/5
        [HttpPut("{id}")]
        public void Put(string id, [FromBody] FilmCreateModel updateModel)
        {
            var films = storageService.GetFilms();
            var film = films.FirstOrDefault(x => x.Id == id);

            if (film == null)
                throw new ArgumentException($"Фильм с идентификатором '{id}' не найден.");

            film.Title = updateModel.Title;
            film.Year = updateModel.Year;
            film.Genre = updateModel.Genre;
            film.Description = updateModel.Description;
            film.Author = updateModel.Author;
            film.CountryAlpha2Code = updateModel.CountryAlpha2Code;
            film.Duration = updateModel.Duration;
            film.Image = updateModel.Image ?? "no_product.png";

            storageService.Save(films);
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(string id)
        {
            var films = storageService.GetFilms();
            var film = films.FirstOrDefault(x => x.Id == id);

            if (film == null)
                throw new ArgumentException($"Фильм с идентификатором '{id}' не найден.");

            //films.Remove(film);

            storageService.Save(films.Where(x => x.Id != id).ToList());
        }

        [HttpPost("upload")]
        public async Task<IActionResult> Post(IFormFile file)
        {

            // Full path to file in temp location
            var image = $"{Guid.NewGuid().ToString()}.{file.FileName.Split('.')[1]}";
            var filePath = Path.Combine(hostingEnvironment.WebRootPath, image);
            
            if (file.Length > 0)
                using (var stream = new FileStream(filePath, FileMode.Create))
                    await file.CopyToAsync(stream);

            // Process uploaded files

            return Ok(new { count = 1, path = image });
        }
    }
}
